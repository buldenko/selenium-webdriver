var mailer = require('./modules/mailer'),
	fs = require('fs'),
	mergeObject = 
	mConfig = JSON.parse(fs.readFileSync('post-config.json', 'utf-8')),
	uConfig = JSON.parse(fs.readFileSync('usermail-info.json', 'utf-8')),
	uAliases = JSON.parse(fs.readFileSync('usermail-aliases.json', 'utf-8'));

let lastHost;

let a = process.argv;
if(a[2] == 'send') {
	if(a[3] != 'all' && a.length > 3) {
		post(a.splice(3));
	}
}

module.exports.mailTask = post;

function post(a, taskConfig) {
	for(let i = 0; i < a.length; i++) {
		if(mConfig[a[i]]) {
			let o = mConfig[a[i]],
				useUserConfig = o.template.useUserInfo === false ? false : true;

			if(!lastHost || (lastHost.auth.user != o.host.auth.user)) {
				mailer.host(o.host);
				lastHost = o.host;
			}
			// if(typeof o.options.to == 'object') {
			// 	for(let u in o.options.to) {
			// 		let opts = {},
			// 			userConfig = {};

			// 		for(let p in o.options) {
			// 			if(p != 'to') {
			// 				opts[p] = o.options[p];
			// 			}
			// 		}
			// 		if(useUserConfig) {
			// 			userConfig = mergeObjects(o.template.config, uConfig[u], true);
			// 		}
			// 		userConfig = mergeObjects(userConfig, o.options.to[u], true);
			// 		if(taskConfig) {
			// 			userConfig = mergeObjects(userConfig, taskConfig);
			// 		}
			// 		opts.to = u;
			// 		mailer.post(o.template.path, userConfig, opts, (err, data) => {
			// 			if(err) {
			// 				console.log(err);
			// 			} else {
			// 				console.log(data);
			// 			}
			// 		});
			// 	}
			// } else if(typeof o.options.to == 'string') {
			// 	var mailConfig = o.template.config;

			// 	if(taskConfig) {
			// 		mailConfig = mergeObjects(mailConfig, taskConfig);
			// 	}
			// 	mailer.post(o.template.path, mailConfig, o.options, (err, data) => {
			// 		if(err) {
			// 			console.log(err);
			// 		} else {
			// 			console.log(data);
			// 		}
			// 	});
			// }

			let mailConfig = o.template.config;

			let opts = {};

			for(let p in o.options) {
				if(p != 'to') {
					opts[p] = o.options[p];
				}
			}

			if(useUserConfig) {
				if(typeof o.options.to == 'object') {
					for(let u in o.options.to) {
						let cfg = [];

						cfg.push(uConfig[u]);
						if(mailConfig) {
							cfg.push(mailConfig);
						}
						cfg.push(o.options.to[u]);
						if(taskConfig) {
							cfg.push(taskConfig);
						}
						sendToUser(o.template.path, cfg, opts, u);
					}
				} else if(typeof o.options.to == 'string') {
					let uArr = o.options.to.split(',');

					uArr.forEach((el, i) => {
						uArr[i] = el.trim();
					});
					for(let u in uArr) {
						let cfg = [];

						cfg.push(uConfig[u]);
						if(mailConfig) {
							cfg.push(mailConfig);
						}
						if(taskConfig) {
							cfg.push(taskConfig);
						}
						sendToUser(o.template.path, cfg, opts, uArr[u]);
					}
				}
			} else {
				if(typeof o.options.to == 'object') {
					for(let u in o.options.to) {
						let cfg = [];

						if(mailConfig) {
							cfg.push(mailConfig);
						}
						cfg.push(o.options.to[u]);
						if(taskConfig) {
							cfg.push(taskConfig);
						}
						sendToUser(o.template.path, cfg, opts, u);
					}
				} else if(typeof o.options.to == 'string') {
					let cfg = [];

					if(mailConfig) {
						cfg.push(mailConfig);
					}
					if(taskConfig) {
						cfg.push(taskConfig);
					}
					sendToUser(o.template.path, cfg, opts, o.options.to);
				}
			}
		}
	}
}
function sendToUser(tPath, cfg, opts, u) {
	let userConfig = {},
		options = {};

	for(let i = 0; i < cfg.length; i++) {
		userConfig = mergeObjects(userConfig, cfg[i], true);
	}
	for(let i in opts) {
		options[i] = opts[i];
	}
	options.to = u;
	mailer.post(tPath, userConfig, options, (err, data) => {
		if(err) {
			console.log(err);
		} else {
			console.log(data);
		}
	});
}
function mergeObjects(a, b, rewrite) {
	var merge;

	if(a instanceof Array) {
		merge = [];
	} else {
		merge = {};
	}
	for(var p in a) {
		if(a[p] instanceof Object) {
			merge[p] = mergeObjects(a[p], b[p] instanceof Object ? b[p] : {}, rewrite);
		} else {
			merge[p] = a[p];
		}
	}
	for(var p in b) {
		if(b[p] instanceof Object) {
			merge[p] = mergeObjects(merge[p], b[p], rewrite);
		} else {
			if(merge[p] === undefined) {
				merge[p] = b[p];
			} else {
				if(rewrite) {
					merge[p] = b[p];
				}
			}
		}
	}

	return merge;
}