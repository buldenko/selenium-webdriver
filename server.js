global.path = __dirname;

var fs = require('fs'),
	express = require('express'),
	apps = {},
	use = require('./server/modules/use'),
	routes = require('./server/modules/routing'),
	routeRequires = require('./server/modules/routeRequires'),
	serverCfg,
	cfgIsLoaded = false,
	config = {},
	defaultPort = 8080;

try {
	serverCfg = JSON.parse(fs.readFileSync('server/server.json', 'utf-8'));
	cfgIsLoaded = true;
} catch(e) {
	console.log(e);
}

if(cfgIsLoaded) {
	for(let appName in serverCfg) {
		let thisConfig = serverCfg[appName],
			app = apps[appName] = express();

		app._appName = appName;
		if(!thisConfig.port) {
			thisConfig.port = defaultPort++;
		} else {
			defaultPort = thisConfig.port + 1;
		}
		if(!thisConfig.host) {
			thisConfig.host = 'localhost';
		}
		if(use.length !== 0) {
			if(serverCfg.all) {
				serverCfg.all.use.forEach((useName) => {
					if(use[useName]) {
						app.use(function(req, res, next) {
							use[useName](req, res, next, routeRequires);
						});
				}
				});
			}
			if(thisConfig.use) {
				thisConfig.use.forEach((useName) => {
					if(use[useName]) {
						app.use(function(req, res, next) {
							use[useName](req, res, next, routeRequires);
						});
					}
				});
			}
		}
		routes.init(app, routeRequires);
		app.listen(thisConfig.port, thisConfig.host, function () {
			console.log(`Application '${app._appName}' listening on port ${thisConfig.port}!`);
		});
	}
}

