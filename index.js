var webdriver = require('selenium-webdriver');
var browser = new webdriver.Builder().usingServer().withCapabilities({'browserName': 'chrome' }).build();
var mail = require('./post.js');
browser.get('https://hpromise.hyundai.ru/online-otsenka/');

pickCarParam('mark', 'Hyundai', 200, 1500).then(function(resolve) {
	return pickCarParam('model', 'Accent', 200, 2000);
}).then(function(resolve) {
	return pickCarParam('production_year', '2004', 200, 2000);
}).then(function(resolve) {
	return pickCarParam('generation', 'II (TagAZ)', 200, 2000);
}).then(function(resolve) {
	return pickCarParam('engine', '1.3 MT/75/Бензин/Передний', 200, 2000);
}).then(function(resolve) {
	return pickCarParam('complectation', 'максимальный', 200, 2000);
}).then(function(resolve) {
	return pickCarParam('body_type', 'Седан', 200, 2000);
}).then(function(resolve) {
	return pickCarParam('state', 'идеальное', 200, 2000);
}).then(function(resolve){
	text_area = browser.findElement(webdriver.By.id('mileage_estimated_car'));
	text_area.then(function(){
		text_area.click().then(function(){
			text_area.sendKeys("300").then(function(){
				text_area.sendKeys(webdriver.Key.ENTER);
				setTimeout(get_result, 2000);
			});

		});
		setTimeout(()=>{}, 5000);	
	});
	
});


function pickCarParam(selectName, optionValue, interval, interval_2) {
	return new Promise(function(resolve, reject) {
		var selectTitle = browser.findElement(webdriver.By.css('#' + selectName + '_estimated_car + .drop-it-down-custom .drop-it-down-custom__title'));
		
		selectTitle.then(function() {
			selectTitle.click();
			setTimeout(function() {
				var option = browser.findElement(webdriver.By.css('.drop-it-down-custom--open [data-value="' + optionValue + '"]'));

				option.then(function() {
					option.click();

					setTimeout(resolve, interval_2);
				});
			}, interval);
		}).catch(reject);
	});
}


function get_result(){
	var price_result = browser.findElement(webdriver.By.id('cost_estimated_car'));
	price_result.then(function(){
		var price_num = parseInt(price_result.getAttribute("innerHTML").replace(' ', ''));

		price_result.getAttribute("innerHTML").then(function(innerHTML) {
			var price_num = parseInt(innerHTML);

			console.log(price_result.getAttribute("innerHTML"));
			if (price_num!=NaN) {
				if (price_num>0) {
					mail.mailTask(['ok_mail'], {
						price: price_num
					});
					browser.quit();
				}else{
					mail.mailTask(['bug_mail'], {
						surname: 'Bla-bloff'
					});
					browser.quit();
				}
			}else{
				mail.mailTask(['bug_mail'], {
					surname: 'Bla-bloff'
				});
				browser.quit();
			}
		});
	})
}

//element_brand = browser.findElement(webdriver.By.css('#mark_estimated_car + .drop-it-down-custom .drop-it-down-custom__title'));
//console.log(element_brand)
//element_brand.then(function() {
//	element_brand.click();
//	setTimeout(function() {
//		element_brand_hyundai = browser.findElement(webdriver.By.css('.drop-it-down-custom--open [data-value="Hyundai"]'));
//		element_brand_hyundai.then(function() {
//			element_brand_hyundai.click();
//			console.log(1)
//			setTimeout(function() {
//				element_model = browser.findElement(webdriver.By.css('#model_estimated_car + .drop-it-down-custom .drop-it-down-custom__title'));
//				element_model.then(function() {
//					element_model.click();
//					console.log(2)
//					setTimeout(function() {
//						element_model_accent = browser.findElement(webdriver.By.css('.drop-it-down-custom--open [data-value="Accent"]'));
//						element_model_accent.then(function() {
//							element_model_accent.click();
//							setTimeout(()=>{}, 10000)
//							browser.takeScreenshot().then(function(i, e) {
//								require('fs').writeFile('./screenshots/out.png', i, 'base64', function(err) {
//						            console.log(err);
//						        });
//						        require('./post.js').mailTask(['testMail'], { surname: 'Bla-bloff' });
//							})
//						});
//					}, 1000);
//				}).catch(error => {
//				    console.log('error' );
//				});
//			}, 1000);
//
//
//		});
//	}, 1000);
//}).then(function() {
//
//}).catch(error => {
//    console.log('error' );
//});