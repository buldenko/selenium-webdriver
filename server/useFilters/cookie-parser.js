module.exports = function(req, res, next, requires) {
	if(req.headers.cookie) {
		var splitCookies = req.headers.cookie.split(';');

		req.cookies = {};
		for(let i = 0; i < splitCookies.length; i++) {
			var cookie = splitCookies[i].trim().split('=');

			req.cookies[cookie[0]] = cookie[1];
		}
	}
	next();
}