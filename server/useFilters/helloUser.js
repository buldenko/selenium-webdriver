module.exports = function(req, res, next, requires) {
	req.beforeString = requires.cursiveText(req.beforeString ? req.beforeString + 'Hello, User! ' : 'Hello, User! ');
	next();
}