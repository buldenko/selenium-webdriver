var swig  = require(global.path + '/modules/swig-template');

swig.basePath(global.path + '/build/html/');

module.exports = swig;