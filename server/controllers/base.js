function base(req, res, next, requires) {
//	res.send(req.beforeString ? req.beforeString + 'Hello World!' : 'Hello World!');
	res.cookie('session_id', new Date().getTime());
	res.send(requires.Html.render('base.html', {
		pageTitle: 'Main page',
		title: '"Hello, world" in the templated page.',
		text: 'This is description text.'
	}));
}
function account(req, res, next, requires) {
	res.send(requires.Html.render('base.html', {
		pageTitle: 'Account',
		title: '"Hello, world" in the pseudo account.',
		text: 'This is description text.'
	}));
}

module.exports = {
	base: base,
	account: account
};
