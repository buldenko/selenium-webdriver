var requiresPath = __dirname + '/../routeRequires/',
	requiresList = {};

try {
	require("fs").readdirSync(requiresPath).forEach(function(file) {
		requiresList[file.replace('.js', '')] = require(requiresPath + file);
	});
} catch(e) {
	console.log(e);
}

module.exports = requiresList;