var controllersPath = __dirname + '/../controllers/',
	controllerList = {};

try {
	require("fs").readdirSync(controllersPath).forEach(function(file) {
		controllerList[file.replace('.js', '')] = require(controllersPath + file);
	});
} catch(e) {
	console.log(e);
}

module.exports = controllerList;