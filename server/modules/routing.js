var routes = require('../routes'),
	controllers = require('./controllers');

module.exports.init = function(app, requires) {
	var rList = routes.list[app._appName];

	for(let r in rList) {
		let thisRoute = rList[r],
			callbacks = [];

		for(let cEl = 0; cEl < thisRoute.controllers.length; cEl++) {
			for(let c in thisRoute.controllers[cEl]) {
				let thisCallbacks = thisRoute.controllers[cEl][c];

				for(let tc = 0; tc < thisCallbacks.length; tc++) {
					callbacks.push(function(req, res, next) {
						controllers[c][thisCallbacks[tc]](req, res, next, requires);
					});
				}
			}
		}

		app[thisRoute.method.toLowerCase()](thisRoute.route, callbacks);
	}
}