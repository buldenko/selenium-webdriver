var filtersPath = __dirname + '/../useFilters/',
	filters = {};

try {
	require("fs").readdirSync(filtersPath).forEach(function(file) {
		filters[file.replace('.js', '')] = require(filtersPath + file);
	});
} catch(e) {
	console.log(e);
}
module.exports = filters;