var routeList = {
	'test server': {
		base: {
			route: '/',
			method: 'GET',
			controllers: [
				{
					base: ['base']
				}
			]
		},
		account: {
			route: '/account',
			method: 'GET',
			controllers: [
				{
					base: ['account']
				}
			]
		}
	}
};

module.exports.list = routeList;