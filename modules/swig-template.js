var swig  = require('swig-templates'),
	templateStorage = {},
	basePath = '';

module.exports.render = function(path, cfg) {
	var t;

	if(templateStorage[path]) {
		t = templateStorage[path];
	} else {
		t = templateStorage[basePath + path] = swig.compileFile(basePath + path);
	}

	return t(cfg);
}
module.exports.basePath = function(path) {
	basePath = path;
}