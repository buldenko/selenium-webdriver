var html = require('./swig-template.js'),
	post = require('./email.js');

module.exports.host = function(o) {
	post.transporter(o);
}
module.exports.options = function(o) {
	post.mailOptions(o);
}
module.exports.send = function(handler) {
	post.send(handler);
}
module.exports.post = function(path, cfg, opts, handler) {
	opts.html = html.render('./mails/' + path, cfg);
	post.mail(opts, handler);
}